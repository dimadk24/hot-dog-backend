# Generated by Django 2.1.2 on 2018-10-14 09:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('clean', '0005_auto_20181014_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='cleantask',
            name='is_read_by_user',
            field=models.BooleanField(default=False),
        ),
    ]
