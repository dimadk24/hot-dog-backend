# Generated by Django 2.1.2 on 2018-10-09 09:57

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('clean', '0002_cleantask_public'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cleantask',
            name='end_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
