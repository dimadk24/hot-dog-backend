from django.contrib.admin import ModelAdmin
from django.utils.html import format_html

from clean.models import CleanTask


class CleanTaskAdmin(ModelAdmin):
    fields = ('public', 'user', 'start_time', 'end_time', 'number_deleted', 'status_text',
              'error', 'is_read_by_user')
    readonly_fields = ('user',)

    def user(self, obj: CleanTask):
        user = obj.public.user
        # noinspection HtmlUnknownTarget
        return format_html('<a href="{url}">{text}</a>',
                           url=user.get_absolute_url(),
                           text=str(user))
