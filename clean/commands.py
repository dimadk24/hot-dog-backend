from typing import List

from clean.models import CleanTask
from main.commands import get_dog_ids, get_vk_api
from main.models import Public


def clean(public: Public) -> None:
    dog_ids = get_dog_ids(public)
    remove_users(public, dog_ids)


def remove_users(public: Public, user_ids: List[int]) -> None:
    task = CleanTask(public=public)
    task.save()
    api = get_vk_api(public.user.access_token)
    for user_id in user_ids:
        api.groups.remove_user(group_id=public.vk_id, user_id=user_id)
        task.number_deleted += 1
        task.save()
    task.end()
