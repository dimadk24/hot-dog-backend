from django.db import models
from django.utils import timezone


class CleanTask(models.Model):
    status_text_choices = (
        ('starting', 'Запускаем'),
        ('searching_for_dogs', 'Ищем собачек'),
        ('removing_dogs', 'Удаляем собачек'),
        ('finished', 'Завершили'),
        ('error', 'Возникла ошибка')
    )
    starting_status_text_choice = status_text_choices[0][0]
    searching_status_text_choice = status_text_choices[1][0]
    removing_status_text_choice = status_text_choices[2][0]
    finished_status_text_choice = status_text_choices[3][0]
    error_status_text_choice = status_text_choices[4][0]
    public = models.ForeignKey('main.Public', on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(blank=True, null=True)
    number_deleted = models.IntegerField(default=0)
    status_text = models.CharField(default=status_text_choices[0][0],
                                   choices=status_text_choices, max_length=100)
    error = models.TextField(blank=True)
    is_read_by_user = models.BooleanField(default=False)

    def end(self, success=True):
        self.status_text = self.finished_status_text_choice if success else \
            self.error_status_text_choice
        self.end_time = timezone.now()
        self.save()

    def __str__(self):
        return f'User: {self.public.user}, Public: {self.public.vk_id}, status: {self.status_text}'

    @property
    def progress(self):
        try:
            return int((self.number_deleted / self.public.dogs) * 100)
        except ZeroDivisionError:
            return 100
