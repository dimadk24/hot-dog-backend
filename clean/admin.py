from django.contrib import admin

# Register your models here.
from clean.admins import CleanTaskAdmin
from clean.models import CleanTask

admin.site.register(CleanTask, CleanTaskAdmin)
