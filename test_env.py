import os
from unittest import TestCase

from django.utils.crypto import get_random_string

from setup_env import Environment


def get_secret_key() -> str:
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    return get_random_string(50, chars)


MOCK_SETTINGS_FILE = 'mock_settings.ini'
MOCK_SECRET_KEY = get_secret_key()
MOCK_SETTINGS = ('[DEFAULT]\n' +
                 f'SECRET_KEY={MOCK_SECRET_KEY}\n' +
                 'DEBUG=0\n' +
                 'DATABASE_URL=sqlite:///:memory:\n' +
                 'VK_APP_ID=2345')


def write_mock_settings():
    with open(MOCK_SETTINGS_FILE, 'w', encoding='utf8') as f:
        f.write(MOCK_SETTINGS)


def remove_mock_settings():
    os.unlink(MOCK_SETTINGS_FILE)


class TestEnv(TestCase):
    def setUp(self):
        write_mock_settings()
        self.env = Environment(MOCK_SETTINGS_FILE)

    def tearDown(self):
        remove_mock_settings()

    def test_secret_key(self):
        expected = MOCK_SECRET_KEY
        actual = self.env.get_str('SECRET_KEY')
        self.assertEqual(expected, actual)

    def test_database_url(self):
        expected = 'sqlite:///:memory:'
        actual = self.env.get_str('DATABASE_URL')
        self.assertEqual(expected, actual)

    def test_debug(self):
        expected = False
        actual = self.env.get_bool('DEBUG')
        self.assertEqual(expected, actual)

    def test_int(self):
        expected = 2345
        actual = self.env.get_int('VK_APP_ID')
        self.assertEqual(expected, actual)

    def test_returns_none_if_doesnt_in_config(self):
        expected = [0, False, '']
        actual = [self.env.get_int('missed'), self.env.get_bool('missed'),
                  self.env.get_str('missed')]
        self.assertEqual(expected, actual)
