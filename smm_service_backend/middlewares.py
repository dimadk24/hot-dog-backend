import json
from json import JSONDecodeError

from django.contrib.auth import authenticate


class VkTokenAuthMiddleware:
    vk_user_id_field = 'user_vk_id'
    vk_auth_key_field = 'auth_key'

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            user_vk_id, auth_key = self.get_auth_data(request)
            request.user = authenticate(request, vk_id=user_vk_id, auth_key=auth_key)
        finally:
            return self.get_response(request)

    def get_auth_data(self, request):
        if request.body:
            try:
                data_dict = json.loads(request.body)
            except JSONDecodeError:
                raise KeyError
        else:
            data_dict = request.GET
        return (
            data_dict[self.vk_user_id_field],
            data_dict[self.vk_auth_key_field]
        )


class DisableCSRFMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)
        response = self.get_response(request)
        return response
