from django.contrib.auth.admin import UserAdmin as BaseAdmin
from django.utils.translation import gettext_lazy as _

from main.models import User


class UserAdmin(BaseAdmin):
    fieldsets = (
        (None,
         {'fields': ('username', 'password', 'vk_id', 'access_token',
                     'balance', 'get_deleted_dogs_count')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    ordering = ['date_joined']
    list_display = ('pk', 'vk_id', 'publics_number')
    readonly_fields = ('get_deleted_dogs_count',)
    view_on_site = False

    def publics_number(self, obj: User):
        return obj.public_set.count()

    security_placeholder = '*' * 10

    def get_access_token(self, obj: User):
        return self.security_placeholder if obj.access_token else 'Не задан'

    get_access_token.short_description = 'Токен доступа'

    def get_deleted_dogs_count(self, obj: User):
        return obj.get_deleted_dogs_count()

    get_deleted_dogs_count.short_description = 'Количество удаленных собачек'
