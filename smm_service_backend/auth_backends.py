from django.contrib.auth import login

from main.commands import get_or_create_user, user_vk_id_is_valid
from main.models import User


class VkTokenAuthBackend:
    def authenticate(self, request, vk_id: int = None, auth_key: str = None):
        if not (vk_id and auth_key and user_vk_id_is_valid(vk_id, auth_key)):
            return None
        user = get_or_create_user(vk_id)
        login(request, user, backend='smm_service_backend.auth_backends.VkTokenAuthBackend')
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
