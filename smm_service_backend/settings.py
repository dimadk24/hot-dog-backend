"""
Django settings for smm_service_backend project.

Generated by 'django-admin startproject' using Django 2.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os

import dj_database_url
import sentry_sdk
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration

from setup_env import Environment

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
env = Environment(os.path.join(BASE_DIR, 'settings.ini'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.get_str('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.get_bool('DEBUG')

if not DEBUG:
    sentry_sdk.init(
        dsn=env.get_str('SENTRY_DSN'),
        integrations=[DjangoIntegration()]
    )

ALLOWED_HOSTS = [
    'hot-dog.site',
    'www.hot-dog.site',
    '127.0.0.1'
]

# Application definition

INSTALLED_APPS = [
    'corsheaders',
    'rest_framework',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'main.apps.MainConfig',
    'clean.apps.CleanConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'smm_service_backend.middlewares.DisableCSRFMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'smm_service_backend.middlewares.VkTokenAuthMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'smm_service_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'smm_service_backend.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASE_URL = env.get_str('DATABASE_URL')
DATABASES = {'default': dj_database_url.parse(DATABASE_URL)}

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'main.User'

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

TEST_VK_ACCESS_TOKEN = env.get_str('TEST_VK_ACCESS_TOKEN')
TEST_VK_ID = env.get_int('TEST_VK_ID')

AUTHENTICATION_BACKENDS = [
    'smm_service_backend.auth_backends.VkTokenAuthBackend',
    'django.contrib.auth.backends.ModelBackend'
]

CORS_ORIGIN_WHITELIST = (
    '127.0.0.1:8000',
    'localhost:3000',
    'dimadk24.github.io',
    'dima24kmagic.github.io'
)

VK_APP_ID = env.get_int('VK_APP_ID')
VK_APP_SECRET_KEY = env.get_str('VK_APP_SECRET_KEY')
VK_APP_ACCESS_TOKEN = env.get_str('VK_APP_ACCESS_TOKEN')
TEST_VK_APP_ID = env.get_int('TEST_VK_APP_ID')
TEST_VK_APP_SECRET_KEY = env.get_str('TEST_VK_APP_SECRET_KEY')
ADMIN_VK_ACCESS_TOKEN = env.get_str('ADMIN_VK_ACCESS_TOKEN')

YANDEX_MONEY_SECRET_KEY = env.get_str('YANDEX_MONEY_SECRET_KEY')

COST_OF_DELETING = float(env.get_str('COST_OF_DELETING'))