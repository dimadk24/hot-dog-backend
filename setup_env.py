import configparser

SETTINGS_FILE = 'settings.ini'


class Environment:
    def __init__(self, file=SETTINGS_FILE, section='DEFAULT'):
        self.config = configparser.ConfigParser()
        self.section = section
        self.read_and_set_config(file)

    def read_and_set_config(self, file):
        with open(file, encoding='utf8') as f:
            self.config.read_file(f)

    def get_bool(self, item):
        return self.config.getboolean(self.section, item, raw=True, fallback=False)

    def get_str(self, item):
        return self.config.get(self.section, item, raw=True, fallback='')

    def get_int(self, item):
        return self.config.getint(self.section, item, raw=True, fallback=0)
