from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase

from main.commands import get_dogs_count, get_public_vk_data
from main.models import Public, User


def mock_vk_groups_getter(mock):
    instance = mock.return_value
    api = instance.get_api.return_value
    api.groups.get.return_value = {
        'items': [{'id': 123, 'name': 'Тестовая страница', 'members_count': 531,
                   'photo_100': 'https://pp.userapi.com/c841122/v841122831/22ef7'
                                '/WrJVP268xVk.jpg?ava=1'}]}
    api.groups.getMembers.return_value = {'items': [{
        'id': 12,
        'deactivated': 'deleted'
    }, {
        'id': 123
    }]}


class PublicTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('test', 'test@ya.ru', 'testPassword',
                                             vk_id=settings.TEST_VK_ID,
                                             access_token=settings.TEST_VK_ACCESS_TOKEN)
        self.public = Public(user=self.user, vk_id=12)

    def test_if_pass_only_id_it_gets_all_data_before_save(self):
        public = self.public
        public_data = get_public_vk_data(public)
        self.assertIn('https://', public_data['avatar_url'])
        self.assertGreater(public_data['followers'], 0)
        self.assertNotEqual(public_data['name'], '')

    def test_updates_dogs_count(self):
        public = self.public
        self.assertNotEqual(get_dogs_count(public), 0)


class MiscellaneousTests(TestCase):
    def test_public_repr(self):
        user = get_user_model().objects.create_user('test', 'test@ya.ru', 'testpassword',
                                                    vk_id=settings.TEST_VK_ID,
                                                    access_token=settings.TEST_VK_ACCESS_TOKEN)
        public = Public(user=user, vk_id=12315,
                        avatar_url='https://pp.vk.com/3435.png',
                        name='Тестовая страница', followers=132, dogs=11)
        self.assertNotIn('deleted', repr(public))
        public.is_active = False
        self.assertIn('deleted', repr(public))
