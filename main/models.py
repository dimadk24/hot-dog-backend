from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.timezone import now

from clean.models import CleanTask


class User(AbstractUser):
    access_token = models.CharField(max_length=150, default='', blank=True)
    vk_id = models.BigIntegerField(default=0)
    clean_capacity = models.IntegerField(default=2000)
    balance = models.FloatField(default=0)

    def get_absolute_url(self):
        return f'/admin/main/user/{self.pk}'

    def get_deleted_dogs_count(self) -> int:
        tasks = CleanTask.objects.filter(public__user=self)
        deleted_dogs_count = 0
        for task in tasks:
            deleted_dogs_count += task.number_deleted
        return deleted_dogs_count


class Public(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vk_id = models.BigIntegerField(unique=True, db_index=True)
    added_time = models.DateTimeField()
    avatar_url = models.URLField(default='')
    name = models.CharField(max_length=100, default='')
    followers = models.IntegerField(default=0)
    dogs = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __repr__(self):
        return (f'{self.vk_id} — {self.name} user: {self.user}, dogs: {self.dogs},'
                f'followers: {self.followers} {"deleted" if not self.is_active else ""}')

    def __str__(self):
        return repr(self)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.added_time = now()
        return super().save(*args, **kwargs)
