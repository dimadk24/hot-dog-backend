import hashlib

from django.conf import settings
from django.http import Http404
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, \
    get_object_or_404, DestroyAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from clean.models import CleanTask
from main.commands import get_public_vk_data, get_dogs_count, count_clean_price
from main.models import Public, User
from main.serializers import PublicSerializer, CleanTaskSerializer, \
    UserSerializer
from main.tasks import clean


class GetPublicsApiView(ListAPIView):
    serializer_class = PublicSerializer

    def get_queryset(self):
        return Public.objects.filter(user=self.request.user, is_active=True)


class AddPublicApiView(CreateAPIView):
    serializer_class = PublicSerializer

    def create(self, request, *args, **kwargs):
        possible_existing_queryset = Public.objects.filter(user=self.request.user,
                                                           vk_id=request.data['vk_id'],
                                                           is_active=False)
        if possible_existing_queryset.exists():
            public = possible_existing_queryset.get()
            public.is_active = True
            public_data = get_public_vk_data(public)
            public.followers = public_data['followers']
            public.avatar_url = public_data['avatar_url']
            public.name = public_data['name']
            public.dogs = 0
            public.save()
            serializer = self.get_serializer(public)
        else:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer: PublicSerializer):
        instance: Public = serializer.save(user=self.request.user)
        public_data = get_public_vk_data(instance)
        instance.followers = public_data['followers']
        instance.avatar_url = public_data['avatar_url']
        instance.name = public_data['name']
        instance.save()


class GetDogsCountApiView(APIView):
    lookup_url_kwarg = 'id'
    lookup_field = 'pk'

    def get_queryset(self):
        return Public.objects.filter(user=self.request.user, is_active=True)

    def get(self, request):
        queryset = self.get_queryset()
        request_data = request.GET
        if self.lookup_url_kwarg in request_data:
            filter_kwargs = {self.lookup_field: request_data[self.lookup_url_kwarg]}
        else:
            raise Http404(f'Missing required parameter: {self.lookup_url_kwarg}')
        public = get_object_or_404(queryset, **filter_kwargs)
        public.dogs = get_dogs_count(public)
        public.save()
        return Response({'dogs_count': public.dogs})


class RefreshPublicApiView(RetrieveAPIView):
    serializer_class = PublicSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'

    def get_queryset(self):
        return Public.objects.filter(user=self.request.user, is_active=True)

    def _get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        request_data = self.request.GET
        if self.lookup_url_kwarg in request_data:
            filter_kwargs = {self.lookup_field: request_data[self.lookup_url_kwarg]}
        else:
            raise Http404(f'Missing required parameter: {self.lookup_url_kwarg}')
        obj = get_object_or_404(queryset, **filter_kwargs)
        self.check_object_permissions(self.request, obj)
        return obj

    def get_object(self):
        public = self._get_object()
        fresh_data = get_public_vk_data(public)
        public.name = fresh_data['name']
        public.followers = fresh_data['followers']
        public.name = fresh_data['name']
        public.dogs = get_dogs_count(public)
        public.save()
        return public


class DeletePublicApiView(DestroyAPIView):
    serializer_class = PublicSerializer
    lookup_url_kwarg = 'id'
    lookup_field = 'pk'

    def get_queryset(self):
        return Public.objects.filter(user=self.request.user, is_active=True)

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        request_data = self.request.data
        if self.lookup_url_kwarg in request_data:
            filter_kwargs = {self.lookup_field: request_data[self.lookup_url_kwarg]}
        else:
            raise Http404(f'Missing required parameter: {self.lookup_url_kwarg}')
        obj = get_object_or_404(queryset, **filter_kwargs)
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_destroy(self, instance: Public):
        instance.is_active = False
        instance.save()


class StartCleanTasksApiView(CreateAPIView):
    def create(self, request, *args, **kwargs):
        if not request.user.access_token:
            return Response({
                'error': {
                    'id': 1,
                    'text': 'access token is not set'
                },
                'solution': 'call /setAccessToken'}, status=status.HTTP_200_OK)
        public_ids = request.data['public_ids']
        price = count_clean_price(public_ids)
        if price > request.user.balance:
            money_difference = round(price - request.user.balance, 2)
            return Response({
                'error': {
                    'id': 2,
                    'text': 'not enough money',
                    'value': money_difference
                },
                'solution': 'add money'
            }, status=status.HTTP_200_OK)
        clean_tasks = []
        for public_id in public_ids:
            clean_tasks.append(CleanTask(public_id=public_id))
        response = []
        for clean_task in clean_tasks:
            clean_task.save()
            clean.delay(clean_task.pk)
            response.append({
                'public_id': clean_task.public.vk_id,
                'clean_task_id': clean_task.pk,
            })
        return Response(response, status=status.HTTP_201_CREATED)


class GetCleanTasksApiView(ListAPIView):
    serializer_class = CleanTaskSerializer

    def get_queryset(self):
        tasks = CleanTask.objects.filter(public__user=self.request.user, is_read_by_user=False)
        for task in tasks:
            if task.status_text in ('finished', 'error'):
                task.is_read_by_user = True
                task.save()
        return tasks


class SetAccessTokenApiView(UpdateAPIView):
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


class AddMoney(UpdateAPIView):
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        return self.patch(request, args, kwargs)

    def patch(self, request, *args, **kwargs):
        label: str = request.data['label']
        codepro = request.data.get('codepro', 'false')
        if not (label.startswith('hot-dog: ') and
                codepro == 'false' and
                request.data.get('unaccepted', 'false') == 'false' and
                request.data['currency'] == '643'):
            return Response([], status.HTTP_200_OK)
        try:
            secret_string = request.data['notification_type'] + '&' + \
                            request.data['operation_id'] + '&' + \
                            request.data['amount'] + '&' + \
                            request.data['currency'] + '&' + \
                            request.data['datetime'] + '&' + \
                            request.data['sender'] + '&' + \
                            codepro + '&' + \
                            settings.YANDEX_MONEY_SECRET_KEY + '&' + \
                            label
            sha1 = hashlib.sha1(secret_string.encode()).hexdigest()

            if request.data['sha1_hash'] != sha1:
                return Response([], status.HTTP_400_BAD_REQUEST)
            user = get_object_or_404(User, vk_id=label[9:])
            user.balance += float(request.data['withdraw_amount'])
            user.save()

        except MultiValueDictKeyError:
            return Response([], status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response([], status.HTTP_200_OK)


class GetBalanceApiView(RetrieveAPIView):
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        return Response({'balance': round(request.user.balance, 2)})


@api_view(['POST'])
def withdraw_money_view(request):
    withdraw_amount = request.data['amount']
    if withdraw_amount < 0:
        return Response({'error': {
            'id': 4,
            'text': 'amount can\'t less than 0, you can\'t add money via this'
        }})
    user = request.user
    if user.balance < withdraw_amount:
        return Response({'error': {
            'id': 3,
            'text': 'not enough money to withdraw',
            'balance': round(user.balance, 2)
        }})
    user.balance -= withdraw_amount
    user.save()
    return Response({
        'ok': 'ok',
        'balance': round(user.balance, 2)
    })
