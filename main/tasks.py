import os
from typing import List

import django
import sentry_sdk
from celery import Celery
from sentry_sdk.integrations.celery import CeleryIntegration
from vk_api import vk_api, VkTools, VkApiError

from setup_env import Environment

env = Environment()

os.environ['DJANGO_SETTINGS_MODULE'] = env.get_str('DJANGO_SETTINGS_MODULE')
django.setup()

from clean.models import CleanTask
from main.models import Public
from django.conf import settings

sentry_sdk.init(
    dsn=env.get_str('SENTRY_DSN'),
    integrations=[CeleryIntegration()]
)

app = Celery(__name__)


@app.task
def clean(clean_task_id: int):
    clean_task = CleanTask.objects.get(pk=clean_task_id)
    public = clean_task.public
    clean_task.status_text = clean_task.searching_status_text_choice
    clean_task.save()
    try:
        dog_ids = get_dog_ids(public)
    except VkApiError as e:
        clean_task.error = e.args
        clean_task.end(False)
        raise
    clean_task.status_text = clean_task.removing_status_text_choice
    clean_task.save()
    try:
        remove_users(public, clean_task.id, dog_ids)
    except VkApiError as e:
        clean_task = CleanTask.objects.get(pk=clean_task_id)
        clean_task.error = e.args
        clean_task.end(False)
        raise
    clean_task = CleanTask.objects.get(pk=clean_task_id)  # get fresh data with number_deleted
    # otherwise it saves old data with number_deleted = 0 and override fresh one
    clean_task.end()


def remove_users(public: Public, clean_task_id: int, user_ids: List[int]) -> None:
    clean_task = CleanTask.objects.get(pk=clean_task_id)
    user = public.user
    api = get_vk_api(user.access_token)
    for user_id in user_ids:
        user.balance -= settings.COST_OF_DELETING
        if user.balance <= 0:
            break
        api.groups.remove_user(group_id=public.vk_id, user_id=user_id)
        clean_task.number_deleted += 1
        clean_task.save()
        user.save()


def get_vk_api(access_token: str):
    return vk_api.VkApi(token=access_token, api_version='5.85').get_api()


def get_dog_ids(public: Public) -> List[int]:
    api = get_vk_api(public.user.access_token)
    followers = VkTools.get_all_iter(api, 'groups.getMembers', max_count=1000,
                                     values={
                                         'group_id': public.vk_id,
                                         'fields': 'deactivated'}
                                     )
    dogs = []
    for follower in followers:
        if 'deactivated' in follower.keys():
            dogs.append(follower['id'])
    return dogs
