# Generated by Django 2.1.1 on 2018-09-28 09:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0005_auto_20180928_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='public',
            name='added_time',
            field=models.DateTimeField(),
        ),
    ]
