from django.urls import path

from . import views

urlpatterns = [
    path('addPublic', views.AddPublicApiView.as_view()),
    path('getDogsCount', views.GetDogsCountApiView.as_view()),
    path('getPublics', views.GetPublicsApiView.as_view()),
    path('refreshPublic', views.RefreshPublicApiView.as_view()),
    path('deletePublic', views.DeletePublicApiView.as_view()),
    path('startCleanTasks', views.StartCleanTasksApiView.as_view()),
    path('getCleanTasks', views.GetCleanTasksApiView.as_view()),
    path('setAccessToken', views.SetAccessTokenApiView.as_view()),
    path('addMoney', views.AddMoney.as_view()),
    path('getBalance', views.GetBalanceApiView.as_view()),
    path('withdrawMoney', views.withdraw_money_view)
]
