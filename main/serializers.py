from rest_framework import serializers

from clean.models import CleanTask
from main.models import Public, User


class PublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Public
        fields = ('id', 'vk_id', 'avatar_url', 'name', 'followers', 'dogs')
        extra_kwargs = {'id': {'source': 'pk'}}


class CleanTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = CleanTask
        fields = ('public_id', 'progress', 'status')
        extra_kwargs = {'status': {'source': 'get_status_text_display'}}


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('access_token',)
