import hashlib
from typing import List, Dict

import vk_api
from django.conf import settings
from vk_api import VkTools

from main.models import Public, User


class GetPublicError(Exception):
    pass


def convert_publics_from_vk_format(publics: List) -> List[Public]:
    converted_publics = []
    for public in publics:
        converted_publics.append(
            Public(
                vk_id=public['id'],
                name=public['name'],
                avatar_url=public['photo_100'],
                followers=public['members_count'],
            )
        )
    return converted_publics


def get_fresh_user_publics(user: User) -> List[Public]:
    json_publics = get_publics_from_vk_as_json(user)
    publics = convert_publics_from_vk_format(json_publics)
    for public in publics:
        public.user = user
        public.dogs = get_dogs_count(public)
    return publics


def get_dogs_count(public: Public) -> int:
    return len(get_dog_ids(public))


def get_publics_from_vk_as_json(user: User) -> List[Dict]:
    api = get_vk_api(user.access_token)
    try:
        json_publics = api.groups.get(user_id=user.vk_id, extended=True,
                                      filter='admin',
                                      fields='members_count')['items']
    except vk_api.exceptions.VkApiError:
        raise GetPublicError
    return json_publics


def get_vk_api(access_token: str = settings.VK_APP_ACCESS_TOKEN):
    return vk_api.VkApi(token=access_token, api_version='5.85').get_api()


def set_user_publics(publics: List) -> None:
    for public in publics:
        Public.objects.update_or_create(vk_id=public.vk_id,
                                        defaults={
                                            'user': public.user,
                                            'avatar_url': public.avatar_url,
                                            'name': public.name,
                                            'dogs': public.dogs,
                                            'followers': public.followers,
                                            'is_active': public.is_active,
                                        })


def get_cached_user_publics(user: User) -> List[Public]:
    return list(Public.objects.filter(user=user))


def get_dog_ids(public: Public) -> List[int]:
    api = vk_api.VkApi(token=settings.ADMIN_VK_ACCESS_TOKEN, api_version='5.85')
    followers = VkTools(api).get_all_iter('groups.getMembers', max_count=1000,
                                          values={
                                              'group_id': public.vk_id,
                                              'fields': 'deactivated'
                                          })
    dogs = []
    for follower in followers:
        if 'deactivated' in follower.keys():
            dogs.append(follower)
    return dogs


def get_or_create_user(vk_id: int) -> User:
    return User.objects.get_or_create(vk_id=vk_id, defaults={'username': vk_id})[0]


def get_vk_id(api):
    return api.users.get()[0]['id']


def get_public_vk_data(public: Public):
    api = get_vk_api()
    response = api.groups.get_by_id(group_id=public.vk_id, fields='members_count')[0]
    return {
        'name': response['name'],
        'followers': response['members_count'],
        'avatar_url': response['photo_100']
    }


def _user_vk_id_is_valid(user_vk_id: int, auth_key: str, is_test: bool) -> bool:
    hash_string = get_hash_string(user_vk_id) if not is_test else get_test_hash_string(user_vk_id)
    validating_hash = make_vk_hash(hash_string)
    return validating_hash == auth_key


def user_vk_id_is_valid(*args) -> bool:
    return _user_vk_id_is_valid(*args, True) or _user_vk_id_is_valid(*args, False)


def make_vk_hash(hash_string):
    return hashlib.md5(hash_string.encode('utf-8')).hexdigest()


def get_hash_string(user_id: int) -> str:
    return format_hash_string(user_id, settings.VK_APP_ID, settings.VK_APP_SECRET_KEY)


def get_test_hash_string(user_id: int) -> str:
    return format_hash_string(user_id, settings.TEST_VK_APP_ID, settings.TEST_VK_APP_SECRET_KEY)


def format_hash_string(user_id: int, app_id: int, app_secret_key: str):
    return f'{app_id}_{user_id}_{app_secret_key}'


def count_clean_price(public_ids: List[int]) -> float:
    overall_dogs = get_overall_dogs_count(public_ids)
    return overall_dogs * settings.COST_OF_DELETING


def get_overall_dogs_count(public_ids):
    overall_dogs = 0
    for public_id in public_ids:
        public = Public.objects.get(pk=public_id)
        overall_dogs += public.dogs
    return overall_dogs
