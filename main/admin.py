from django.contrib import admin
from django.contrib.auth.models import Group

from main.models import Public, User
from smm_service_backend.admins import UserAdmin

# Register your models here.

admin.site.register(User, UserAdmin)
admin.site.register(Public)
admin.site.unregister(Group)
