URL for getting test access_token and getting user id (vk_id):
https://oauth.vk.com/authorize?client_id=5644185&redirect_uri=https://oauth.vk.com/blank.html&display=page&response_type=token&v=5.85&scope=groups,offline

Good public with plenty dogs: https://vk.com/wedding_dedovich

settings.ini format:
[DEFAULT]
SECRET_KEY=
DEBUG=
DATABASE_URL=
TEST_VK_ACCESS_TOKEN=
TEST_VK_ID=
SENTRY_DSN=
VK_APP_ID=
VK_APP_SECRET_KEY=
VK_APP_ACCESS_TOKEN=
TEST_VK_APP_ID=
TEST_VK_APP_SECRET_KEY=
YANDEX_MONEY_SECRET_KEY=